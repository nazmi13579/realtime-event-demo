import React, { Component, createRef } from 'react';
import { w3cwebsocket as W3CWebSocket } from 'websocket'
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css'
import 'react-tiny-fab/dist/styles.css';
import moment from 'moment';

const client = new W3CWebSocket('ws://127.0.0.1:8000?token=' + localStorage.getItem('realtimeevent.token'))

class Events extends Component {

    constructor(props) {
        super(props)

        this.newevent = {}

        this.state = {
            showEditor: false,
            selectedevent: {},
            events: {}
        }

        this.formref = createRef()
        this.editref = createRef()
    }

    shouldComponentUpdate (last, news) {
        console.log(news)
        return true
    }

    updateEvents () {
        return fetch('http://localhost:8000/events', {
          method: 'GET'
        })
          .then(data => data.json())
          .then((res)=>{
              var eventsObject = {}
              for (const event of res) {
                  eventsObject[event._id] = event
              }
              console.log('first events', eventsObject)
              this.setState({events: eventsObject})
            })
          .catch(err=>{
              alert('Error')
          })
    }

    componentDidMount () {
        this.updateEvents()
        client.onopen = () => {
            console.log("Websocket client connected")
          }
        client.onmessage = (m) => {
            console.log("onmessage", m)
            const message = JSON.parse(m.data)
            console.log("got reply! ", message)

            // for add & update only
            const event = message.message

            if (message.type === 'addevent') this.setState(prevState => ({
                events: {
                    ...prevState.events,
                    [event._id]: event 
                }
            }))
            else if (message.type === 'updateevent') this.setState(prevState => ({
                events: {
                    ...prevState.events,
                    [event._id]: event 
                }
            }))
            else if (message.type === 'deleteevent') {
                var events = this.state.events
                delete events[message.message._id]
                this.setState({events: events})
            }
        }
        client.onclose = () => {
            alert("Error: websocket closed!")
        }
    }

    sendMessage = (type, message) => {
        console.log("Sending message")
        client.send(JSON.stringify({
          type: type,
          msg: message
        }))
      }

    onSubmit = (e) => {
        e.preventDefault()
        
        fetch('http://localhost:8000/events', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(this.newevent)
        })
          .then(data => {
              if (data.status === 201) {
                alert('Event added')
                this.formref.current.close()
              }
              else
                alert('Error: ' + data.statusText)
            })
          .catch(err=>{
              alert('Error')
          })
    }

    onUpdate = (e) => {
        e.preventDefault()
        
        fetch('http://localhost:8000/events/' + this.state.selectedevent._id, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(this.state.selectedevent)
        })
          .then(data => {
              console.log("data", data)
              if (data.status === 200) {
                alert('Event updated')
                this.editref.current.close()
              }
              else
                alert('Error: ' + data.statusText)
            })
          .catch(err=>{
              alert('Error')
          })
    }

    onDelete = (event) => {
        
        fetch('http://localhost:8000/events/' + event._id, {
          method: 'DELETE'
        })
          .then(data => {
              console.log("data", data)
              if (data.status === 200) {
                alert('Event deleted')
              }
              else
                alert('Error: ' + data.statusText)
            })
          .catch(err=>{
              alert('Error')
          })
    }

    formatDate (date) {
        console.log('format', date)
        if (date)
            return moment(date).format('D MMMM YYYY')
        else return ""
    }

    formatTime (time) {
        console.log('format time', time)
        if (time)
            return moment(time, 'HH:mm').format('h:mm a')
        else return ""
    }

    render() {
        return (
            <div>
                <h3 style={{margin: '20px'}}>Events</h3>
                <div>
                    {Object.keys(this.state.events).map((key, i)=>{
                        return (
                            <div className='mybox clickable' key={this.state.events[key]._id}>
                                <div className='eventTitle'>{this.state.events[key].title}</div>
                                <div className='eventDate'>{this.formatDate(this.state.events[key].date)}</div>
                                <div className='eventTime'>{this.formatTime(this.state.events[key].time)}</div>
                                <div className='eventVenue'>{this.state.events[key].venue}</div>
                                
                                <Popup trigger={<button className='mybutton'>Option</button>} position="bottom center">
                                    <div>
                                        <button className='mybuttonoption greybutton' onClick={()=>{
                                            this.editref.current.open()
                                            this.setState({selectedevent: this.state.events[key]})
                                            }}>Edit</button>
                                        
                                        <button className='mybuttonoption redbutton' onClick={()=>this.onDelete(this.state.events[key])}>Delete</button>
                                    </div>
                                </Popup>
                                <Popup ref={this.editref} modal nested>
                                    <div>
                                        <form className='flexCenter' onSubmit={this.onUpdate} style={{padding:'20px'}}>
                                            <h3 style={{margin: '10px'}}>Edit Event</h3>
                                            <table style={{width:'100%'}}>
                                                <tbody>
                                                    <tr>
                                                        <td>Title</td>
                                                        <td><input type="text" placeholder='Title' value={this.state.selectedevent.title} className='form-control' onChange={(e)=>this.setState({selectedevent: {...this.state.selectedevent, title: e.target.value}})} /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Date</td>
                                                        <td><input type="date" placeholder='Date' value={this.state.selectedevent.date} className='form-control' onChange={(e)=>this.setState({selectedevent: {...this.state.selectedevent, date: e.target.value}})} /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Time</td>
                                                        <td><input type="time" placeholder='Time' value={this.state.selectedevent.time} className='form-control' onChange={(e)=>this.setState({selectedevent: {...this.state.selectedevent, time: e.target.value}})} /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Venue</td>
                                                        <td><input type="text" placeholder='Venue' value={this.state.selectedevent.venue} className='form-control' onChange={(e)=>this.setState({selectedevent: {...this.state.selectedevent, venue: e.target.value}})} /></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <button type='submit' className='mybutton'>Save</button>
                                        </form>
                                        
                                    </div>
                                </Popup>
                            </div>
                        )
                    })}
                </div>
                <button className='myfab' onClick={()=>{
                        this.newevent = {
                            title: '',
                            date: '',
                            time: '',
                            venue: ''
                        }
                        this.formref.current.open()
                    }}>+</button>
                <Popup ref={this.formref} modal nested>
                    <form className='flexCenter' onSubmit={this.onSubmit} style={{padding:'20px'}}>
                        <h3 style={{margin: '10px'}}>New Event</h3>
                        <table style={{width:'100%'}}>
                            <tbody>
                                <tr>
                                    <td>Title</td>
                                    <td><input type="text" placeholder='Title' className='form-control' onChange={(e)=>this.newevent.title = e.target.value} /></td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td><input type="date" placeholder='Date' className='form-control' onChange={(e)=>this.newevent.date = e.target.value} /></td>
                                </tr>
                                <tr>
                                    <td>Time</td>
                                    <td><input type="time" placeholder='Time' className='form-control' onChange={(e)=>this.newevent.time = e.target.value} /></td>
                                </tr>
                                <tr>
                                    <td>Venue</td>
                                    <td><input type="text" placeholder='Venue' className='form-control' onChange={(e)=>this.newevent.venue = e.target.value} /></td>
                                </tr>
                            </tbody>
                        </table>
                        <button type='submit' className='mybutton'>Add</button>
                    </form>
                </Popup>
            </div>
        );
    }
}

export default Events;
