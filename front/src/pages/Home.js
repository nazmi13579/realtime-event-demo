import { Navigate } from 'react-router-dom'

function Home() {
    if (localStorage.getItem('realtimeevent.loggedin')==='true')
        return (
            <div>
                This is home
            </div>
        )
    else
        return <Navigate to="/login" />
}

export default Home