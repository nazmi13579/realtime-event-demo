import React from "react"

class Login extends React.Component {

    state = { username: '', password: '', user: {} }

    constructor(props) {
        super(props)

        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async loginUser(credentials) {
        return fetch('http://localhost:8000/login', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(credentials)
        })
          .then(data => {
              console.log(data.status)
              if (data.status === 200)
                return data.json()
              else
                alert('Error: ' + data.statusText)
            })
          .catch(err=>{
              alert('Error')
          })
       }

    handleSubmit = async (e) => {
        e.preventDefault()
        const token = await this.loginUser({username: this.state.username, password: this.state.password})
        console.log('get token: ', token)
        localStorage.setItem('realtimeevent.token', token.token)
        if (token) window.location.href='/events'
    }

    render () {
        return (
            <div className="mybox" style={{width: '400px'}}>
                <h3 style={{margin:'20px'}}>Login</h3>
                <form onSubmit={this.handleSubmit}>
                    <input className="form-control" onChange={e=>{this.setState({username:e.target.value})}} style={{marginBottom:'10px'}} type="text" placeholder="Username" />
                    <input className="form-control" onChange={e=>{this.setState({password:e.target.value})}} type="password" placeholder="Password" />
                    <button className="mybutton" type="submit">Go</button>
                </form>
            </div>
        )
    }
}

export default Login