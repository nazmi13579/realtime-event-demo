import './App.css';
import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Home from './pages/Home'
import Login from './pages/Login'
import Events from './pages/Events'
import './style.css'
import './bootstrap/dist/css/bootstrap.min.css'

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {apiResponse:''}
  }

  render () {
    console.log('rendering')
    return (
      <Router>
        <div className='App flexCenter'>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/events" element={<Events/>} />
          </Routes>
        </div>
      </Router>
    )
  }
}

export default App;
