class UserManager {

    static users = [
        {
            id: 1,
            username: 'A',
            password: 'AA',
            token: 'atoken'
        },
        {
            id: 2,
            username: 'B',
            password: 'BB',
            token: 'btoken'
        }
    ]

    static getByCredentials(credentials) {
        return this.users.find(user=>user.username === credentials.username && user.password === credentials.password)
    }

    static getByToken (token) {
        return this.users.find(user=>user.token === token)
    }

    static getAll () {
        return this.users
    }
}

module.exports = UserManager