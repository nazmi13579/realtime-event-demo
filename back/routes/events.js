var express = require("express")
var router = express.Router()
var WebSocketManager = require("../managers/websocketManager")

const Event = require('../models/event')

router.post('/', async (req, res) => {
    const event = new Event(req.body)
    try {
        const newEvent = await event.save()
        res.status(201).json(newEvent)
        WebSocketManager.onAddEvent(event)
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

router.get('/', async (req, res) => {
    let events
    try {
        events = await Event.find()
        res.json(events)
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

router.put('/:id', getEvent, async (req, res) => {
    let newevent = req.body
    try {
        await Event.findByIdAndUpdate(req.params.id, newevent)
        res.json(newevent)
        WebSocketManager.onUpdateEvent(newevent)
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

router.delete('/:id', async (req, res) => {
    let event
    try {
        event = await Event.findById(req.params.id)
        if (event == null)
            res.status(404).json({ message: 'Cannot find event' })
        else {
            event.remove()
            res.json({ message: 'Deleted event' })
            WebSocketManager.onDeleteEvent(event)
        }
    } catch (err) {
        res.status(500).json({ message: err.message})
    }
})

async function getEvent(req, res, next) {
    let event
    try {
        event = await Event.findById(req.params.id)
        if (event == null) {
            return res.status(404).json({message: 'Event not found'})
        }
    } catch (err) {
        return res.status(500).json({message: err.message})
    }
    res.event = event
    next()
}

module.exports = router